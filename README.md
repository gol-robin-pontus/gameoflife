# Project for course DT042G

The purpose of this project is to document and test a working version of Game of Life by Erik Ström.

This project is a collaboration by
* Robin Östlund (rost1400)
* Pontus Törngren (potr1400).

**Trello board:** https://trello.com/b/aiZ9hRPX/gameoflife
