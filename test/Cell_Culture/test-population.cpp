#include "catch.hpp"

#include <string>

#include "Cell_Culture/Population.h"

SCENARIO("creating a Population") {
    GIVEN("a new Population object") {
        Population population;
        
        WHEN("global variable fileName doesn't point to a cell culture file") {
            fileName = "";

            AND_WHEN("initiating with an empty rule string") {
                REQUIRE_NOTHROW(population.initiatePopulation(""));
            }

            AND_WHEN("initiating with equal rule strings") {
                REQUIRE_NOTHROW(population.initiatePopulation("von_neumann", "erik"));
            }

            THEN("population size should match default world dimensions") {
                population.initiatePopulation("");
                REQUIRE(population.getTotalCellPopulation() == ((WORLD_DIMENSIONS.WIDTH + 2) * (WORLD_DIMENSIONS.HEIGHT + 2)));
            }

            AND_WHEN("compared to another randomized Population") {
                population.initiatePopulation("conway");
                population.calculateNewGeneration();

                Population population_b;
                population_b.initiatePopulation("conway");
                population_b.calculateNewGeneration();

                THEN("the cell population should differ") {
                    bool different = false;

                    // compare non-rim cells until a mismatching cell pair is encountered
                    for (int x = 1; x <= WORLD_DIMENSIONS.WIDTH && !different; x++) {
                        for (int y = 1; y <= WORLD_DIMENSIONS.HEIGHT && !different; y++) {
                            auto
                                cell_a = population.getCellAtPosition({x, y}),
                                cell_b = population_b.getCellAtPosition({x, y});

                            // a Cell == Cell operator would be convenient
                            if (cell_a.isAlive() != cell_b.isAlive()) {
                                different = true;
                            } else {
                                if (cell_a.isAliveNext() != cell_b.isAliveNext()) {
                                    different = true;
                                } else {
                                    if (cell_a.getColor() != cell_b.getColor()) {
                                        different = true;
                                    } else {
                                        if (cell_a.getCellValue() != cell_b.getCellValue()) {
                                            different = true;
                                        } else {
                                            if (cell_a.getNextGenerationAction() != cell_b.getNextGenerationAction()) {
                                                different = true;
                                            } else {
                                                if (cell_a.getAge() != cell_b.getAge()) {
                                                    different = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } // for y
                    } // for x

                    // there's a possibility that two random populations actually are identical
                    CHECK(different);
                }
            }
        }

        WHEN("global variable fileName points to non-existent file") {
            fileName = "this_shouldnt_exist.file";

            THEN("initiating shouldn't throw an exception") {
                CHECK_NOTHROW(population.initiatePopulation("erik"));
            }
        }

        WHEN("global variable fileName points to provided Population_Seed.txt") {
            fileName = "Population_Seed.txt";

            THEN("population size should be (20+2) x (10+2) = 264") {
                REQUIRE_NOTHROW(population.initiatePopulation("erik"));
                REQUIRE(population.getTotalCellPopulation() == 264);
            }

            AND_WHEN("initiating with Conway rules") {
                REQUIRE_NOTHROW(population.initiatePopulation("conway"));

                AND_WHEN("calculating new generation") {
                    int generation;
                    REQUIRE_NOTHROW(generation = population.calculateNewGeneration());

                    THEN("calculateNewGeneration returns the generation number") {
                        REQUIRE(generation == 1);

                        AND_THEN("calling it again returns the next number") {
                            REQUIRE(population.calculateNewGeneration() == (generation + 1));

                            AND_WHEN("re-initiating the population") {
                                REQUIRE_NOTHROW(population.initiatePopulation("conway"));

                                THEN("cells {7,2} and {4,8} have no scheduled action") {
                                    REQUIRE(population.getCellAtPosition({7,2}).getNextGenerationAction() == DO_NOTHING);
                                    REQUIRE(population.getCellAtPosition({4,8}).getNextGenerationAction() == DO_NOTHING);
                                }
                            }
                        }
                    }

                    THEN("cell {7,2} dies") {
                        REQUIRE(population.getCellAtPosition({7,2}).getNextGenerationAction() == KILL_CELL);
                    }

                    THEN("cell {4,8} resurrects") {
                        REQUIRE(population.getCellAtPosition({4,8}).getNextGenerationAction() == GIVE_CELL_LIFE);
                    }
                }
            }
        }
    }
}
