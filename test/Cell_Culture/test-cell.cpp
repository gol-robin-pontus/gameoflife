#include "catch.hpp"

#include "Cell_Culture/Cell.h"

SCENARIO("Attempting to alter rim cell") {
    GIVEN("a rim cell") {
        WHEN("a rim cell is created") {
            Cell rimCell(true);

            THEN("isRimCell function should return true") {
                REQUIRE(rimCell.isRimCell() == true);
            }

            THEN("isAlive function should return false") {
                REQUIRE(rimCell.isAlive() == false);
            }

            AND_WHEN("action of rim cell is altered") {
                ACTION originalAction = rimCell.getNextGenerationAction();
                rimCell.setNextGenerationAction(GIVE_CELL_LIFE);

                THEN("original action should be the same as action after update") {
                    REQUIRE(rimCell.getNextGenerationAction() == originalAction);
                }
            }
        }
    }
}

SCENARIO("Altering a non-rim cell") {
    GIVEN("a non-rim cell") {
        WHEN("a non-rim cell is created with no action") {
            Cell cell(false);

            THEN("it should not be a rim cell") {
                REQUIRE(cell.isRimCell() == false);
            }

            THEN("action should be DO_NOTHING") {
                REQUIRE(cell.getNextGenerationAction() == DO_NOTHING);
            }

            THEN("isAlive should return false") {
                REQUIRE(cell.isAlive() == false);
            }

            THEN("age should be 0") {
                REQUIRE(cell.getAge() == 0);
            }

            THEN("color should be dead") {
                REQUIRE(cell.getColor() == STATE_COLORS.DEAD);
            }

            THEN("cell value should be #") {
                REQUIRE(cell.getCellValue() == '#');
            }

            AND_WHEN("cell action is changed to GIVE_CELL_LIFE and next color is set to living") {
                cell.setNextGenerationAction(GIVE_CELL_LIFE);
                cell.setNextColor(STATE_COLORS.LIVING);

                THEN("getNextGenerationAction should be GIVE_CELL_LIFE") {
                    REQUIRE(cell.getNextGenerationAction() == GIVE_CELL_LIFE);
                }

                AND_WHEN("cell is updated") {
                    cell.updateState();

                    THEN("isAlive should return true") {
                        REQUIRE(cell.isAlive() == true);
                    }

                    THEN("age should be 1") {
                        REQUIRE(cell.getAge() == 1);
                    }

                    THEN("color should be living") {
                        REQUIRE(cell.getColor() == STATE_COLORS.LIVING);
                    }

                    THEN("next cell action should be DO_NOTHING") {
                        REQUIRE(cell.getNextGenerationAction() == DO_NOTHING);
                    }

                    AND_WHEN("next cell value is set to % and cell is updated again") {
                        cell.setNextCellValue('%');
                        cell.updateState();

                        THEN("isAlive should return true") {
                            REQUIRE(cell.isAlive() == true);
                        }

                        THEN("cell value should be %") {
                            REQUIRE(cell.getCellValue() == '%');
                        }

                        THEN("age should still be 1") {
                            REQUIRE(cell.getAge() == 1);
                        }
                    }

                    AND_WHEN("cell action is changed to IGNORE_CELL and cell is updated") {
                        cell.setNextGenerationAction(IGNORE_CELL);
                        cell.updateState();

                        THEN("age should be 2") {
                            REQUIRE("cell.getAge() == 2");
                        }
                    }

                    AND_WHEN("cell action is changed to KILL_CELL and cell is updated") {
                        cell.setNextGenerationAction(KILL_CELL);
                        cell.updateState();

                        THEN("age should be 0 and isAlive should return false") {
                            REQUIRE(cell.getAge() == 0);
                            REQUIRE(cell.isAlive() == false);
                        }

                        AND_WHEN("isAliveNext is set to true") {
                            cell.setIsAliveNext(true);

                            THEN("isAliveNext should return true") {
                                REQUIRE(cell.isAliveNext() == true);
                            }
                        }
                    }
                }
            }
        }
    }
}
