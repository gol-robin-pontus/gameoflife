#include "catch.hpp"

#include "GoL_Rules/RuleOfExistence_Conway.h"
#include "GoL_Rules/RuleOfExistence_Erik.h"
#include "GoL_Rules/RuleOfExistence_VonNeumann.h"
#include "GoL_Rules/RuleFactory.h"

#include <iostream>

//Test variables
RuleOfExistence* conway;
RuleOfExistence* erik;
RuleOfExistence* neumann;

//Main cell map
std::map<Point, Cell> cells;

//Copy that can be altered by classes then reset to original
std::map<Point, Cell> bCells = cells;

SCENARIO("Matching outcome of different rulesets with expected outcome") {
    GIVEN("three different rulesets") {
        //Populate cell map in a manner fit for testing, 10x10
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                if (x == 0 || x == 9 || y == 0 || y == 9)
                    cells[Point{x, y}] = Cell(true);
                else
                    cells[Point{x, y}] = Cell(false);
            }
        }

        //Set up alive cells for testing

        cells[Point{1, 1}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{3, 1}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{2, 2}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{2, 4}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{3, 4}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{4, 4}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{3, 5}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{3, 6}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{7, 6}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{6, 7}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{7, 7}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{8, 7}] = Cell(false, GIVE_CELL_LIFE);
        cells[Point{7, 8}] = Cell(false, GIVE_CELL_LIFE);

        //Update all cells
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                cells.at(Point{x, y}).updateState();
            }
        }

        bCells = cells;

        conway = RuleFactory::getInstance().createAndReturnRule(bCells, "conway");
        erik = RuleFactory::getInstance().createAndReturnRule(bCells, "erik");
        neumann = RuleFactory::getInstance().createAndReturnRule(bCells, "von_neumann");     

        THEN("names should match rule") {
            REQUIRE(conway->getRuleName() == "conway");
            REQUIRE(erik->getRuleName() == "erik");
            REQUIRE(neumann->getRuleName() == "von_neumann");
        }

        THEN("actions of all cells should be DO_NOTHING") {
            REQUIRE(bCells.at(Point{1, 3}).getNextGenerationAction() == DO_NOTHING);
        }


        //Conway specific tests
        WHEN("Conway rule is applied") {
            conway->executeRule();

            THEN("cell 2,1 should be dead with next action GIVE_CELL_LIFE") {
                REQUIRE(bCells.at(Point{2,1}).isAlive() == false);
                REQUIRE(bCells.at(Point{2,1}).getNextGenerationAction() == GIVE_CELL_LIFE);
            }

            THEN("cell 3,5 should be alive with next action KILL_CELL") {
                REQUIRE(bCells.at(Point{3,5}).isAlive() == true);
                REQUIRE(bCells.at(Point{3,5}).getNextGenerationAction() == KILL_CELL);
            }

            THEN("cell 7,7 should be alive with next action KILL_CELL") {
                REQUIRE(bCells.at(Point{7,7}).isAlive() == true);
                REQUIRE(bCells.at(Point{7,7}).getNextGenerationAction() == KILL_CELL);
            }

            AND_WHEN("all cells are updated") {
                //Update all cells
                for (int x = 0; x < 10; x++) {
                    for (int y = 0; y < 10; y++) {
                        bCells.at(Point{x, y}).updateState();
                    }
                }

                THEN("cell 2,1 should be alive from resurrection") {
                    REQUIRE(bCells.at(Point{2,1}).isAlive() == true);
                }

                THEN("cell 3,5 should be dead from overpopulation") {
                    REQUIRE(bCells.at(Point{3,5}).isAlive() == false);
                }

                THEN("cell 7,7 should be dead from overpopulation") {
                    REQUIRE(bCells.at(Point{7,7}).isAlive() == false);
                }
            }
        }

        //Reset cells
        bCells = cells;

        //Von Neumann specific tests
        WHEN("Von Neumann rule is applied") {
            neumann->executeRule();

            THEN("cell 2,1 should be dead with next action GIVE_CELL_LIFE") {
                REQUIRE(bCells.at(Point{2,1}).isAlive() == false);
                REQUIRE(bCells.at(Point{2,1}).getNextGenerationAction() == GIVE_CELL_LIFE);
            }

            THEN("cell 3,5 should be alive with next action IGNORE_CELL") {
                REQUIRE(bCells.at(Point{3,5}).isAlive() == true);
                REQUIRE(bCells.at(Point{3,5}).getNextGenerationAction() == IGNORE_CELL);
            }

            THEN("cell 7,7 should be alive with next action KILL_CELL") {
                REQUIRE(bCells.at(Point{7,7}).isAlive() == true);
                REQUIRE(bCells.at(Point{7,7}).getNextGenerationAction() == KILL_CELL);
            }

            AND_WHEN("all cells are updated") {
                //Update all cells
                for (int x = 0; x < 10; x++) {
                    for (int y = 0; y < 10; y++) {
                        bCells.at(Point{x, y}).updateState();
                    }
                }

                THEN("cell 2,1 should be alive from resurrection") {
                    REQUIRE(bCells.at(Point{2,1}).isAlive() == true);
                }

                THEN("cell 3,5 should be alive") {
                    REQUIRE(bCells.at(Point{3,5}).isAlive() == true);
                }

                THEN("cell 7,7 should be dead from overpopulation") {
                    REQUIRE(bCells.at(Point{7,7}).isAlive() == false);
                }
            }
        }

        //Reset cells
        bCells = cells;

        //Erik specific tests
        WHEN("Erik's rule is applied") {
            erik->executeRule();

            THEN("cell 2,1 should be dead with next action GIVE_CELL_LIFE") {
                REQUIRE(bCells.at(Point{2,1}).isAlive() == false);
                REQUIRE(bCells.at(Point{2,1}).getNextGenerationAction() == GIVE_CELL_LIFE);
            }

            THEN("cell 3,5 should be alive with next action KILL_CELL") {
                REQUIRE(bCells.at(Point{3,5}).isAlive() == true);
                REQUIRE(bCells.at(Point{3,5}).getNextGenerationAction() == KILL_CELL);
            }

            THEN("cell 7,7 should be alive with next action KILL_CELL") {
                REQUIRE(bCells.at(Point{7,7}).isAlive() == true);
                REQUIRE(bCells.at(Point{7,7}).getNextGenerationAction() == KILL_CELL);
            }

            AND_WHEN("all cells are updated") {
                //Update all cells
                for (int x = 0; x < 10; x++) {
                    for (int y = 0; y < 10; y++) {
                        bCells.at(Point{x, y}).updateState();
                    }
                }

                THEN("cell 2,1 should be alive from resurrection") {
                    REQUIRE(bCells.at(Point{2,1}).isAlive() == true);
                }

                THEN("cell 3,5 should be dead from overpopulation") {
                    REQUIRE(bCells.at(Point{3,5}).isAlive() == false);
                }

                THEN("cell 7,7 should be dead from overpopulation") {
                    REQUIRE(bCells.at(Point{7,7}).isAlive() == false);
                }

                AND_WHEN("an elder cell is manually created at age 5 and erik cell is created at age 10") {
                    //Elder cell
                    for (int i = 0; i < 6; i++) {
                        erik->executeRule();
                        bCells.at(Point{1,1}).setNextGenerationAction(GIVE_CELL_LIFE);
                        bCells.at(Point{1,1}).updateState();
                    }
                    //Erik cell
                    for (int i = 0; i < 11; i++) {
                        erik->executeRule();
                        bCells.at(Point{2,1}).setNextGenerationAction(GIVE_CELL_LIFE);
                        bCells.at(Point{2,1}).updateState();
                    }

                    THEN("both cells should be alive") {
                        REQUIRE(bCells.at(Point{1,1}).isAlive() == true);
                        REQUIRE(bCells.at(Point{2,1}).isAlive() == true);
                    }

                    THEN("both cells should have elder color") {
                        REQUIRE(bCells.at(Point{1,1}).getColor() == STATE_COLORS.OLD);
                        REQUIRE(bCells.at(Point{2,1}).getColor() == STATE_COLORS.OLD);
                    }

                    THEN("erik cell value should be E") {
                        REQUIRE(bCells.at(Point{2,1}).getCellValue() == 'E');
                    }
                }
            }
        }
    }
}
