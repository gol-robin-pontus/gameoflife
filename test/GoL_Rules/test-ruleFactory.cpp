#include "catch.hpp"

#include "GoL_Rules/RuleFactory.h"

SCENARIO("creating rules") {
    std::map<Point, Cell> cells;
    for (int x = 1; x < 9; x++) {
        cells[Point{x, 0}] = Cell(true);
        cells[Point{x, 9}] = Cell(true);

        for (int y = 1; y < 9; y++) {
            cells[Point{x, y}] = Cell(false, GIVE_CELL_LIFE);
        }
    }
    for (int y = 0; y < 10; y++) {
        cells[Point{0, y}] = Cell(true);
        cells[Point{9, y}] = Cell(true);
    }

    std::vector<std::string> ruleNames {"conway", "erik", "von_neumann"};
    for (auto ruleName : ruleNames) {
        GIVEN("existing rule name '" + ruleName + "'") {
            THEN("it should create the respective rule") {
                auto rule = RuleFactory::getInstance().createAndReturnRule(cells, ruleName);
                REQUIRE_THAT(rule->getRuleName(), Catch::Equals(ruleName, Catch::CaseSensitive::No));
            }
        }
    }

    ruleNames = {"this_is_not_a_rule", ""};
    for (auto ruleName : ruleNames) {
        GIVEN("non-existent rule name '" + ruleName + "'") {
            THEN("it should fall back to a default rule") {
                auto rule = RuleFactory::getInstance().createAndReturnRule(cells, ruleName);
                REQUIRE_FALSE(rule->getRuleName().empty());
            }
        }
    }
}