#include "catch.hpp"
#include "Support/MainArguments.h"

SCENARIO("Creating different argument types") {
    GIVEN("all different argument types") {
        HelpArgument* help = new HelpArgument();
        GenerationsArgument* gen = new GenerationsArgument();
        WorldsizeArgument* world = new WorldsizeArgument();
        FileArgument* file = new FileArgument();
        EvenRuleArgument* even = new EvenRuleArgument();
        OddRuleArgument* odd = new OddRuleArgument();

        THEN("help should return -h") {
            REQUIRE(help->getValue() == "-h");
        }
        THEN("gen should return -g") {
            REQUIRE(gen->getValue() == "-g");
        }
        THEN("world should return -s") {
            REQUIRE(world->getValue() == "-s");
        }
        THEN("file should return -f") {
            REQUIRE(file->getValue() == "-f");
        }
        THEN("even should return -er") {
            REQUIRE(even->getValue() == "-er");
        }
        THEN("odd should return -or") {
            REQUIRE(odd->getValue() == "-or");
        }

        WHEN("ApplicationValues struct is created") {
            ApplicationValues appVal;
            char c[5];
            strcpy(c, "test");

            AND_WHEN("help is executed") {
                help->execute(appVal, c);

                THEN("appValues.runSimulation should be false") {
                    REQUIRE(appVal.runSimulation == false);
                }
            }

            AND_WHEN("gen is executed with generations value 3") {
                strcpy(c, "3");
                gen->execute(appVal, c);

                THEN("appValues.maxGenerations should be 3") {
                    REQUIRE(appVal.maxGenerations == 3);
                }
            }

            AND_WHEN("world is executed with dimensions value 74x26") {
                strcpy(c, "74x26");
                world->execute(appVal, c);

                THEN("WORLD_DIMENSIONS.WIDTH should be 74 and WORLD_DIMENSIONS.HEIGHT should be 26") {
                    REQUIRE(WORLD_DIMENSIONS.WIDTH == 74);
                    REQUIRE(WORLD_DIMENSIONS.HEIGHT == 26);
                }
            }

            AND_WHEN("file is executed with fileName test") {
                file->execute(appVal, c);

                THEN("fileName should be test") {
                    REQUIRE(fileName == "test");
                }
            }

            AND_WHEN("even is executed with rule name test") {
                even->execute(appVal, c);

                THEN("appVal.evenRuleName should be test") {
                    REQUIRE(appVal.evenRuleName == "test");
                }
            }

            AND_WHEN("odd is executed with rule name test") {
                odd->execute(appVal, c);

                THEN("appVal.oddRuleName should be test") {
                    REQUIRE(appVal.oddRuleName == "test");
                }
            }
        }
        delete help, gen, world, file, even, odd;
    }
}
