#include "catch.hpp"

#include "Support/MainArgumentsParser.h"

SCENARIO("parsing argument strings") {
    // to avoid deprecation warnings
    // see https://stackoverflow.com/a/1519997 for an explanation
    char
            arg0[] = "programName",
            arg_h[] = "-h",
            arg_er[] = "-er",
            arg_or[] = "-or",
            arg_conway[] = "conway",
            arg_erik[] = "erik",
            arg_von_neumann[] = "von_neumann",
            arg_s[] = "-s",
            arg_20x12[] = "20x12",
            arg_80x24[] = "80x24",
            arg_160x24[] = "160x24",
            arg_160x0[] = "160x0",
            arg_160x[] = "160x",
            arg_f[] = "-f",
            arg_population_seed_txt[] = "Population_Seed.txt",
            arg_g[] = "-g",
            arg_0[] = "0",
            arg_24[] = "24",
            arg_80[] = "80",
            arg_200[] = "200",
            arg_1000[] = "1000",
            arg_b[] = "-b",
            arg_this_shouldnt_exist_file[] = "this_shouldnt_exist.file";

    GIVEN("an empty argument list") {
        char* argv[] = {&arg0[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE(appval.runSimulation);
        }
    }

    GIVEN("a valid argument list: -h") {
        char* argv[] = {&arg0[0], &arg_h[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE_FALSE(appval.runSimulation);
        }
    }

    GIVEN("a valid argument list: -s 80x24 -g 200") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_80x24[0], &arg_g[0], &arg_200[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE(appval.runSimulation);
            REQUIRE(appval.maxGenerations == 200);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }
    }

    GIVEN("a valid argument list: -s 160x24 -er erik") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_160x24[0], &arg_er[0], &arg_erik[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE(appval.runSimulation);
            REQUIRE(appval.evenRuleName == "erik");
            REQUIRE(appval.oddRuleName == "erik");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 160);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }
    }

    GIVEN("a valid argument list: -s 160x24 -er conway -or erik -g 1000 -f Population_Seed.txt") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_160x24[0], &arg_er[0], &arg_conway[0], &arg_or[0], &arg_erik[0], &arg_g[0], &arg_1000[0], &arg_f[0], &arg_population_seed_txt[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE(appval.runSimulation);
            REQUIRE(appval.maxGenerations == 1000);
            REQUIRE(appval.evenRuleName == "conway");
            REQUIRE(appval.oddRuleName == "erik");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 160);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            REQUIRE(fileName == "Population_Seed.txt");
        }
    }

    GIVEN("unrecognized argument in an otherwise valid argument list: -s 20x12 -or von_neumann -b 200") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_20x12[0], &arg_or[0], &arg_von_neumann[0], &arg_b[0], &arg_200[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should be parsed correctly") {
            MainArgumentsParser parser;
            auto appval = parser.runParser(argv, argc);

            REQUIRE(appval.runSimulation);
            REQUIRE(appval.oddRuleName == "von_neumann");
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 12);
        }
    }

    GIVEN("an invalid value in the argument list: -g von_neumann") {
        char* argv[] = {&arg0[0], &arg_g[0], &arg_von_neumann[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("an invalid value in the argument list: -s 160x0") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_160x0[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("an invalid value in the argument list: -s 160x") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_160x[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("an invalid value in the argument list: -s 80 24") {
        char* argv[] = {&arg0[0], &arg_s[0], &arg_80[0], &arg_24[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("an invalid value in the argument list: -f this_shouldnt_exist.file") {
        char* argv[] = {&arg0[0], &arg_f[0], &arg_this_shouldnt_exist_file[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("an invalid value in the argument list: -g 0") {
        char* argv[] = {&arg0[0], &arg_g[0], &arg_0[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }

    GIVEN("a missing value in the argument list: -g -s 80x24") {
        char* argv[] = {&arg0[0], &arg_g[0], &arg_s[0], &arg_80x24[0], NULL};
        int argc = (int)(sizeof(argv) / sizeof(argv[0])) - 1;

        THEN("it should throw an exception") {
            MainArgumentsParser parser;
            REQUIRE_THROWS([&](){
                auto appval = parser.runParser(argv, argc);
            }());
        }
    }
}