#include "catch.hpp"
#include "Support/FileLoader.h"

SCENARIO("Loading cell map from file") {
    map<Point, Cell> cells;
    FileLoader f;

    GIVEN("a non-existent file") {
        fileName = "does_not_exist.file";

        THEN("loadPopulationFromFile should throw exception") {
            REQUIRE_THROWS(f.loadPopulationFromFile(cells));
        }
    }

    GIVEN("an existing file") {
        fileName = "Population_Seed.txt";
        f.loadPopulationFromFile(cells);

        THEN("WORLD_DIMENSIONS.WIDTH should be 20 and WORLD_DIMENSIONS.HEIGHT should be 10") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
        }

        THEN("all of first row should be rim cells") {
            REQUIRE(cells.at(Point{2, 0}).isRimCell() == true);
            REQUIRE(cells.at(Point{6, 0}).isRimCell() == true);
            REQUIRE(cells.at(Point{11, 0}).isRimCell() == true);
            REQUIRE(cells.at(Point{17, 0}).isRimCell() == true);
        }

        THEN("all of last row should be rim cells") {
            REQUIRE(cells.at(Point{2, 11}).isRimCell() == true);
            REQUIRE(cells.at(Point{6, 11}).isRimCell() == true);
            REQUIRE(cells.at(Point{11, 11}).isRimCell() == true);
            REQUIRE(cells.at(Point{17, 11}).isRimCell() == true);
        }

        THEN("cell 3, 3 should be alive") {
            REQUIRE(cells.at(Point{3, 3}).isAlive() == true);
        }

        THEN("cell 2, 4 should be alive") {
            REQUIRE(cells.at(Point{2, 4}).isAlive() == true);
        }

        THEN("cell 17, 1 should be alive") {
            REQUIRE(cells.at(Point{18, 2}).isAlive() == true);
        }

        THEN("cell 17, 9 should be alive") {
            REQUIRE(cells.at(Point{17, 9}).isAlive() == true);
        }

        THEN("cell 2, 2 should be dead") {
            REQUIRE(cells.at(Point{2, 2}).isAlive() == false);
        }

        THEN("cell 4, 9 should be dead") {
            REQUIRE(cells.at(Point{4, 9}).isAlive() == false);
        }

        THEN("cell 13, 5 should be dead") {
            REQUIRE(cells.at(Point{13, 5}).isAlive() == false);
        }
    }
}
