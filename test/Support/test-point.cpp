#include "catch.hpp"

#include <algorithm>
#include <vector>
#include "Support/SupportStructures.h"

SCENARIO("sorting a vector of points") {
    GIVEN("a sorted vector of points") {
        std::vector<Point> pts {{6,0}, {0,1}, {6,9}, {7,77}, {77,7}, {77,6}, {-1,8}};
        std::sort(pts.begin(), pts.end());
        
        THEN("point #1 should be {-1,8}") {
            REQUIRE(pts[0].x == -1);
            REQUIRE(pts[0].y == 8);
        }
        
        THEN("point #2 should be {0,1}") {
            REQUIRE(pts[1].x == 0);
            REQUIRE(pts[1].y == 1);
        }
        
        THEN("point #3 should be {6,0}") {
            REQUIRE(pts[2].x == 6);
            REQUIRE(pts[2].y == 0);
        }
        
        THEN("point #4 should be {6,9}") {
            REQUIRE(pts[3].x == 6);
            REQUIRE(pts[3].y == 9);
        }
        
        THEN("point #5 should be {7,77}") {
            REQUIRE(pts[4].x == 7);
            REQUIRE(pts[4].y == 77);
        }
        
        THEN("point #6 should be {77,6}") {
            REQUIRE(pts[5].x == 77);
            REQUIRE(pts[5].y == 6);
        }
        
        THEN("point #6 should be {77,7}") {
            REQUIRE(pts[6].x == 77);
            REQUIRE(pts[6].y == 7);
        }
    }
}