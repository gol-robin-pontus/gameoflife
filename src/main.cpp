/** @file main.cpp
 *
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 */

/** @mainpage Game of Life Robin & Pontus
 *
 * Final project in course DT042G HT17.
 * 
 * The purpose of this project is to document and test a working version of Game of Life.
 *
 * The original author of the code that is to be documented and tested is Erik Ström.
 * 
 * The authors of this project are Robin Östlund (rost1400) and Pontus Törngren (potr1400).
 */

#include <iostream>
#include "GameOfLife.h"
#include "Support/MainArgumentsParser.h"

#ifdef DEBUG
#include <memstat.hpp>
#endif

using namespace std;

int main(int argc, char* argv[]) {

    MainArgumentsParser parser;
    ApplicationValues appValues = parser.runParser(argv, argc);

    if (appValues.runSimulation) {
        // Start simulation
        try {
            GameOfLife gameOfLife = GameOfLife(appValues.maxGenerations, appValues.evenRuleName, appValues.oddRuleName);
            gameOfLife.runSimulation();
        }
        catch(ios_base::failure &e){}

    }

    cout << endl;
    return 0;
}
