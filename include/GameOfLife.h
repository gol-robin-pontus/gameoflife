/** @file GameOfLife.h
 *  @author Erik Ström
 *  @date October 2017
 *  @version 0.1
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/** @class GameOfLife
 * 	@brief The heart of the simulation, interconnects the main execution with the graphical presentation.
 * 	@details Creates and manages Population and the ScreenPrinter instances. Will instruct the Population of cells to keep
 *	updating as long as the specified number of generations is not reached, and for each iteration instruct
 *	ScreenPrinter to write the information on screen.
 *  @test Tests not reasonable; methods used are already tested extensively (where appropriate).
 */
class GameOfLife {

private:
    Population population;
    ScreenPrinter& screenPrinter;
    int nrOfGenerations;

public:

	/** @brief Sets up rules for the game.
	 * 	@details Construct a GameOfLife object to base simulation on.
	 * 	@param nrOfGenerations Number of generations to run simulation for.
	 * 	@param evenRuleName Name of ruleset to be used for even generations.
	 * 	@param oddRuleName Name of ruleset to be used for odd generations.
	 */
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);
	
	/** @brief Run the simulation for as many generations as has been set by the user (default = 500).
	 * 	@details For each iteration; calculate population changes and print the information on screen.
	 */
    void runSimulation();

};

#endif
