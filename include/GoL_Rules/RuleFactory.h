/** @file RuleFactory.h
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/** @class RuleFactory
    @brief Singleton class to handle creation of RulesOfExistence objects
    @details Singleton: Access with `RuleFactory::getInstance()`.
*/
class RuleFactory
{
private:
    RuleFactory() {}

public:
    /** @brief Access the RuleFactory instance (singleton)
        @return RuleFactory instance
    */
    static RuleFactory& getInstance();
    
    /** @brief Create rule for cells
        @param cells Cells the rule should apply on
        @param ruleName Rule to use
        @return Created RuleOfExistence object
        @test Test with existing and non-existant rules
    */
    RuleOfExistence* createAndReturnRule(map<Point, Cell>& cells, string ruleName = "conway");
};

#endif