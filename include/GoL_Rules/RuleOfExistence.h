/**
 * @file RuleOfExistence.h
 * @brief These rules lie at the heart of the simulation, and determines the fate of each cell in the world population.
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 */
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include<string>
#include<map>
#include<vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;

/** @struct PopulationLimits
 *	@brief Data structure for storing population limits. Used by rules to determine what ACTION to make.
 */
struct PopulationLimits {
    int UNDERPOPULATION, // cell dies of loneliness
            OVERPOPULATION, // cell dies of over crowding
            RESURRECTION; // cell lives on / is resurrected
};

/** @struct Directions 
 * 	@brief Data structure for storing directional values. Used by rules to determine neighbouring cells.
 */
struct Directions {
    int HORIZONTAL, VERTICAL;
};

// All directions; N, E, S, W, NE, SE, SW, NW
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

// Cardinal directions; N, E, S, W
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

// Diagonal directions; NE, SE, SW, NW
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/** @class RuleOfExistence
 *	@brief Abstract base class, upon which concrete rules will derive.
 *
 * 	@details The derivations of RuleOfExistence is what determines the culture of Cell Population. Each rule implements
 *	specific behaviours and so may execute some parts in different orders. In order to accommodate this
 * 	requirement RuleOfExistence will utilize a **Template Method** desing pattern, where all derived rules
 *	implements their logic based on the virtual method executeRule().
 */
class RuleOfExistence {
protected:
    string ruleName;

    // Reference to the population of cells
    map<Point, Cell>& cells;

    // Amounts of alive neighbouring cells, with specified limits
    const PopulationLimits POPULATION_LIMITS;

    // The directions, by which neighbouring cells are identified
    const vector<Directions>& DIRECTIONS;

	// Determines the amount of alive neighbouring cells to current cell, using directions specified by the rule.
    int countAliveNeighbours(Point currentPoint);
	
	// Determines what action should be taken regarding the current cell, based on alive neighbouring cells.
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:
	/** @brief Constructs RuleOfExistence object and sets member variables based on input.
	 *	@param limits Amounts of alive neighbouring cells, with specified limits.
	 *	@param cells Reference to the population of cells.
	 * 	@param DIRECTIONS The directions, by which neighbouring cells are identified.
	 * 	@param ruleName Name of the rule.
	 */
    RuleOfExistence(PopulationLimits limits, map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS, string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS), ruleName(ruleName) {}
    virtual ~RuleOfExistence() {}

    /** @brief Execute rule, in order specific to the concrete rule, by utilizing template method DP
	 */
    virtual void executeRule() = 0;

	/** @brief Get name of the rule in use.
	 *  @return String name of rule.
	 * 	@test Create RuleOfExistence object with different names and use this function to get the same name back.
	 */
    string getRuleName() { return ruleName; }
};

#endif