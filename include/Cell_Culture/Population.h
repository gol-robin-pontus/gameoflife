/** @file Population.h
 *	@author Erik Ström
 *	@date October 2017
 *	@version 0.1
 */

#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/** @class Population
 *	@brief Representation of the complete society of cell existence and interactions.
 *	@details The Population constitutes of all current, previous and future generations of cells, both living and dead
 *	as well as those not yet born. By mapping all cells to their respective positions in the simulation world,
 *	Population has the complete knowledge of each cell's whereabouts. Furthermore, the class is responsible for
 *	determining which rules should be required from the RuleFactory, and store the pointer to these as members.
 *
 *	Population's main responsibility during execution is determining which rule to apply for each new generation
 *	and updating the cells to their new states.
 * 	@test Create Population objects with different rules. Use member functions to alter and read values. Compare results to expected outcome.
 *	@test Set global variable fileName to read cell culture from file. Compare to expected outcome. Leave variable empty to randomize cell culture. Run multiple times and assume different outcome.
 */
class Population
{
private:
    int generation;
    map<Point, Cell> cells;
    RuleOfExistence* evenRuleOfExistence;
    RuleOfExistence* oddRuleOfExistence;

	// Build cell culture based on randomized starting values.
    void randomizeCellCulture();
	
	// Send cells map to FileLoader, which will populate its culture based on file values.
    void buildCellCultureFromFile();

public:
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}
    ~Population();

	/** @brief Initializing cell culture and the concrete rules to be used in simulation.
	 *	@param evenRuleName Name of ruleset to be used for even generations.
	 *	@param oddRuleName Name of ruleset to be used for odd generations. Uses evenRuleName if not set.
	 */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");
	
	/** @brief Update the cell population and determine next generational changes based on rules.
	 *	@return Integer value representing the generation the simulation is on.
	 */
    int calculateNewGeneration();

    /** @brief Get the cell at a specified position.
	 * 	@param position Position of requested cell.
	 *	@return Pointer to a Cell object.
	 */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

	/** @brief Gets the total amount of cells in the population, regardless of state.
	 * 	@return Integer value representing the current amount of cells.
	 */
    int getTotalCellPopulation() { return cells.size(); }

};

#endif