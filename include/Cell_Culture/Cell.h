/** @file Cell.h
 *	@author Erik Ström
 *	@date October 2017
 *	@version 0.1
 */

#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

/** @struct StateColors
 *	@brief Data structure holding colors to visualize the state of cells.
 */
struct StateColors {
    COLOR LIVING, // Representing living cell
            DEAD, // Representing dead cell
            OLD,  // Representing old cell
            ELDER;// Representing very old cell
}	/* Initiate default values. */
const STATE_COLORS = { COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA };

// Cell action. Determined by rule, and sent to cell for future change.
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };


/** @class Cell
 *	@brief Cells represent a certain combination of row and column of the simulated world.
 *	
 *	@details Cells may be of two types; rim cells, those representing the outer limits of the world,
 *	or non-rim cells. The first cell type are immutable, exempt from the game's rules, and
 *	thus their values may not be changed. The latter type, however, may be changed and edited
 *	in ways specified by the rules.
 */
class Cell {

private:
    struct CellDetails {	// encapsulate cell details
        int age;
        COLOR color;
        bool rimCell;
        char value;
    } details;

    struct NextUpdate {		// encapsulate changes to next state
        ACTION nextGenerationAction;
        COLOR nextColor;
        char nextValue;
        bool willBeAlive;	// some rules may need to know beforehand whether the cell will be alive
    } nextUpdate;


    void incrementAge() { details.age++; }

    void killCell() { details.age = 0; }

    // Sets the character value of the cell, which will be printed to screen.
    void setCellValue(char value) { details.value = value; }

    void setColor(COLOR color) { this->details.color = color; }

public:

	/** @fn Cell(bool isRimCell = false, ACTION action = DO_NOTHING)
	 *
	 * @brief Constructor that determines the cell's starting values.
	 *
	 * @details Creates a cell with default values except for isRimCell and action which can be altered in constructor call.
	 * If these are not set they will default to false and DO_NOTHING.
	 * 
	 * @param isRimCell Whether the cell is a rim cell and therefore immutable or not.
	 * @param action What action to perform on first update.
	 * 
	 * @test Create different types of cells with different actions and check what happens to cell on first update.
	 */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

	/** @fn isAlive()
	 * @brief Checks if cell is alive
	 *
	 * @details Checks if cell is a rim cell, returns false if it is as these cannot be altered.
	 * If rimCell is false the age of the cell is checked. If age is greater than 0 returns true, otherwise returns false.
	 *
	 * @return True if cell is alive. False if cell is dead.
	 *
	 * @test Create a cell that will be alive and one that will be dead and run function to see if that is the case.
	 */
    bool isAlive();

	/** @fn setNextGenerationAction(ACTION action)
	 *
	 * @brief Sets the cell's next action to take in its coming update.
	 * 
	 * @details Checks if the cell is alive and applies new action if it is. Otherwise returns doing nothing to the cell.
	 *
	 * @param action New action for cell to take in its coming update.
	 *
	 * @test Set different actions and run getNextGenerationAction() to make sure it's correct. Try with rim cells to make sure they are not altered.
	 */
    void setNextGenerationAction(ACTION action);

	/** @fn updateState()
	 *
	 * @brief Updates the cell to its new state, based on stored update values.
	 *
	 * @test Create cells with different update values and run function. Then check if cell updated accordingly using get functions.
	 */
    void updateState();

	/** @fn getAge()
	 *
	 * @brief Get cell age.
	 *
	 * @return Integer value that represents age of the cell.
	 *
	 * @test Create a cell and check age using this function. Do this for a few updates and compare to expected results.
	 */
    int getAge() { return details.age; }

	/** @fn getColor()
	 *
	 * @brief Get cell color.
	 * 
	 * @details Function called to get the color of the cell. Colors represent state of cell.
	 *
	 * @return COLOR value representing current color of the cell.
	 *
	 * @test Create a cell and check color using this function. Do this for a few updates and compare to expected results.
	 */
    COLOR getColor() { return details.color; }

	/** @fn isRimCell()
	 *
	 * @brief Check if cell is rim cell.
	 * 
	 * @details If cell is a rim cell it is immutable.
	 *
	 * @return Returns true if cell is rim cell. False if it is not.
	 *
	 * @test Create rim cell and non-rim cell and compare return value of this function to expected result.
	 */
    bool isRimCell() { return details.rimCell; }

	/** @fn setNextColor(COLOR nextColor)
	 *
	 * @brief Sets color of cell after next update.
	 *
	 * @param nextColor The color to change the cell into on next update.
	 *
	 * @test Set color of the cell then run getColor() to see if color is the same.
	 */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

	/** @fn getCellValue()
	 *
	 * @brief Get cell value.
	 * 
	 * @details Value is the character used to represent a cell printed on the screen.
	 *
	 * @return Char value used to represent the cell.
	 *
	 * @test Set value using setNextCellValue() and run updateState(). Then use this function to see if value is the same as set.
	 */
    char getCellValue() { return details.value; }

	/** @fn setNextCellValue(char value)
	 *
	 * @brief Set character value of the cell on the next update.
	 * 
	 * @details Sets the next character value of the cell on next update. Value is the character used to represent a cell printed on the screen.
	 *
	 * @param value Character to be used to represent the cell when printed on the screen.
	 *
	 * @test Set a value using this function. Run updateState() to update the color then check value using getCellValue().
	 */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

	/** @fn setIsAliveNext(bool isAliveNext)
	 *
	 * @brief Sets whether the cell is alive/dead next generation.
	 *
	 * @param isAliveNext Boolean value determining if cell will be alive or dead next update.
	 *
	 * @test Set the value using this function then get it using isAliveNext(). The starting value should be the same as the return value.
	 */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

	/** @fn isAliveNext()
	 *
	 * @brief Check if cell will be alive next update.
	 *
	 * @return Returns true if cell will be alive next update. False if it will be dead.
	 *
	 * @test Set value using setIsAliveNext() then use this function to get the value. The starting value should be the same as the return value.
	 */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /** @fn getNextGenerationAction()
	 *
	 * @brief Get next action of the cell.
	 *
	 * @return Pointer to the ACTION to be performed next update.
	 *
	 * @test Set action using setNextGenerationAction() and use this function to get the value back. The starting value should be the same as the return value.
	 */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif
