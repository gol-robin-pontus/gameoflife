/** @file Globals.h
    @brief States the existence of global variables
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;

/** @var WORLD_DIMENSIONS
    @brief The width and height of the world
    @details The value constitutes the used dimensions for width and height of the cell culture map.
*/
extern Dimensions WORLD_DIMENSIONS;

/** @var fileName
    @brief Name of cell cuulture file to read
    @details Path to a file containing a cell culture map to use in the simulation.
*/
extern string fileName;


#endif