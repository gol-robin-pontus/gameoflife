/** @file MainArguments.h
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

/** @struct ApplicationValues
    @brief Contains setting values used for the simulation
*/
struct ApplicationValues {
    bool runSimulation = true;
    string evenRuleName, oddRuleName;
    int maxGenerations = 100;

};

/** @class BaseArgument
    @brief Application argument
    @details Constitutes an argument passed to the application
*/
class BaseArgument {
protected:
    const string argValue;

    /** @brief print message on screen stating no value was provided for the argument
    */
    void printNoValue();

public:
    /** @brief Construct argument with value
        @param argValue value of argument
        @test construct a BaseArgument with an arbitrary value for @p argValue and check that getValue() returns this value
    */
    BaseArgument(string argValue) : argValue(argValue) {}
    virtual ~BaseArgument() {}
    
    /** @brief Put argument value into the simulation settings
        @param appValues ApplicationValues object to target
        @param value argument value to insert
        
        @cond DOXYGEN_IGNORE_THIS
        Doxygen will reuse this text for execute in child classes
        @endcond
    */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

    /** @brief Retrieve the argument value
        @return the value of the argument
        @test construct a BaseArgument with an arbitrary value for @p argValue and check that getValue() returns this value
    */
    const string& getValue() { return argValue; }
};

/** @class HelpArgument
    @brief Application argument for help screen
*/
class HelpArgument : public BaseArgument {
public:
    HelpArgument() : BaseArgument("-h") {}
    ~HelpArgument() {}

    void execute(ApplicationValues& appValues, char* value);
};


/** @class GenerationsArgument
    @brief Application argument for the amount of generations to simulate
*/
class GenerationsArgument : public BaseArgument {
public:
    GenerationsArgument() : BaseArgument("-g") {}
    ~GenerationsArgument() {}

    void execute(ApplicationValues& appValues, char* generations);
};

/** @class WorldsizeArgument
    @brief Application argument for custom population size
*/
class WorldsizeArgument : public BaseArgument {
public:
    WorldsizeArgument() : BaseArgument("-s") {}
    ~WorldsizeArgument() {}

    void execute(ApplicationValues& appValues, char* dimensions);
};

/** @class FileArgument
    @brief Application argument for initiating population from file
*/
class FileArgument : public BaseArgument {
public:
    FileArgument() : BaseArgument("-f") {}
    ~FileArgument() {}

    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/** @class EvenRuleArgument
    @brief Application argument for rule used for even generations
*/
class EvenRuleArgument : public BaseArgument {
public:
    EvenRuleArgument() : BaseArgument("-er") {}
    ~EvenRuleArgument() {}

    void execute(ApplicationValues& appValues, char* evenRule);
};

/** @class OddRuleArgument
    @brief Application argument for rule used for odd generations
*/
class OddRuleArgument : public BaseArgument {
public:
    OddRuleArgument() : BaseArgument("-or") {}
    ~OddRuleArgument() {}

    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
