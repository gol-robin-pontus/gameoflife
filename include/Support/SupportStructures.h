/** @file SupportStructures.h
   Various supportive structures to be used throughout the application!
   
   @author Erik Ström
   @date October 2017
   @version 0.1
*/

#ifndef GAMEOFLIFE_SUPPORTSTRUCTURES_H
#define GAMEOFLIFE_SUPPORTSTRUCTURES_H

/** @struct Point
    @brief Constitutes a single Point in the simulated world.
    @details The Point structure handles x and y (column/row) coordinates in the world of Game of life, and is used
    to map Cell objects to their positions.
    
    <b>Example:</b>
	@code
        std::vector<Point> foo {{6,0}, {0,1}, {5,9}};
    @endcode
    
    @test Test by filling a container with Points and sort these using the < operator overload.
*/
struct Point {
    /** @publicsection
        @var x
        @brief the Point's x coordinate
        
        @var y
        @brief the Point's y coordinate
    */
    int x, y;

    /** @fn operator < (const Point& other) const
        @brief compare Point objects
        @details compare a Point object with another for use in e.g. sorting.
        @param other a second Point object to compare with
        @return true if this Point's coordinates are lesser than the coordinates of Point @p other
    */
    bool operator < (const Point& other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

};

/** @struct Dimensions
    @brief World dimensions
    @details Data structure holding information about world dimensions in pixels.
*/
struct Dimensions {
    /** @publicsection
        @var WIDTH
        @brief the Dimensions' width
        
        @var HEIGHT
        @brief the Dimensions' height
    */
    int WIDTH, HEIGHT;
};


#endif //GAMEOFLIFE_SUPPORTSTRUCTURES_H
