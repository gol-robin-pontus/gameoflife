/** @file MainArgumentsParser.h
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/** @class MainArgumentsParser
    @brief Parses the starting arguments passed to the application
    @test Ensure the parsed application values match the passed arguments
    
    <b>Example</b>
    @code
    int main(int argc, char* argv[]) {
        MainArgumentsParser parser;
        ApplicationValues appValues = parser.runParser(argv, argc);
        
        (...)
    @endcode
*/
class MainArgumentsParser {
public:
    /** @brief Retrieve application values from application arguments
        @param argv array of arguments passed to the application
        @param length length of the array of arguments (@p argv)
        @return the application values parsed from the arguments
    */
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues;

    // Checks if a given option exists
    bool optionExists(char** begin, char** end, const std::string& option);

    // gets the given option value
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
