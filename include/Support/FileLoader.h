/** @file FileLoader.h
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/** @class FileLoader
    @brief Reads starting values for simulation
    @details See FileLoader::loadPopulationFromFile(map<Point, Cell>& cells)
*/
class FileLoader {

public:
    FileLoader() {}

    /** @brief Populate cells and set world size
        @details Global variable WORLD_DIMENSIONS and the cell map is populated with values from the file path in global variable fileName.
        @param cells Map of Cells at Points to receive data from file
    
        @test Ensure file data is read correctly
    */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
