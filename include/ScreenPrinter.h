/** @file ScreenPrinter.h
    @author Erik Ström
    @date October 2017
    @version 0.1
*/

#ifndef screenPrinterH
#define screenPrinterH

#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"

/** @class ScreenPrinter
    @brief Responsible for visually representing the simulation world on screen
    @details Singleton: Access with `ScreenPrinter::getInstance()`.
	@test Tests not reasonable; methods used are already tested extensively (where appropriate).
*/
class ScreenPrinter {
private:
    Terminal terminal;

    ScreenPrinter() {}

public:
    /** @brief Access the ScreenPrinter instance (singleton)
        @return ScreenPrinter instance
    */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }

    /** @brief Print board on screen
        @param population population board to print
    */
    void printBoard(Population& population);

    /// @brief Print help screen
    void printHelpScreen();

    /** @brief Print a message on screen
        @param message Message to print
        
        <b>Example:</b>
        @code
        ScreenPrinter::getInstance().printMessage("No value for " + argValue + " found!");
        @endcode
    */
    void printMessage(string message);

    /// @brief Clear screen
    void clearScreen();
};

#endif
